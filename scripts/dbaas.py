from __future__ import print_function, division

##################################################################
########      Copyright (c) 2015-2017 OpenSCG      ###############
##################################################################

import json, os, sys, sqlite3
import util, api

AMI_URL="http://169.254.169.254/latest/meta-data/"
try:
  # For Python 3.0 and later
  from urllib import request as urllib2
except ImportError:
  # Fall back to Python 2's urllib2
  import urllib2

DEVOPS_DB = os.getenv("PGC_HOME") + os.sep + "data" + os.sep + "pgdevops" + os.sep + "devops.db"
try:
  con = sqlite3.connect(DEVOPS_DB, check_same_thread=False)
except Exception as error:
  pass


def get_field(p_dict, p_field1, p_field2=None):
   try:
     if p_field2 is None:
       return(str(p_dict[p_field1]))
     else:
       return(str(p_dict[p_field1][p_field2]))
   except:
     return ""


def cloudlist(p_isJSON, p_group="*", p_user="*", p_area="*", p_key="*"):
  sql = "SELECT user_group, user_name, cd_area, cd_key, \n" + \
        "       cd_level cd_value date_updated \n" + \
        "  FROM cloud_defaults \n" + \
        "ORDER BY 1, 2, 3, 4, 5, 6"
  print(sql)



def dblist(p_isJSON, p_isVERBOSE, p_list_type, p_region, p_instance, p_email):
  preferred_list_types = ["postgres", "rds"]
  list_type = p_list_type.lower()

  if list_type in ("pg", "postgres", "postgresql"):
    return(pglist(p_isJSON, p_isVERBOSE, p_email=p_email))
  elif list_type == "rds":
    return(rdslist(p_isJSON, p_isVERBOSE, p_region, p_instance, p_email, "postgres"))
  else:
    msg = p_list_type + " not valid for dbaas.dblist()." + "  \n try: " + str(preferred_list_types)
    util.exit_message(msg, 1, p_isJSON)
  

def pglist(p_isJSON, p_isVERBOSE, p_region="", p_email=""):
  try:
    c = con.cursor()
    sql = "SELECT u.email, g.name as server_group, s.name as server_name, \n" + \
          "       s.host, s.port, s.maintenance_db as db, s.username as db_user, \n" + \
          "       s.id as sid, g.id as gid, s.password as pwd \n" + \
          "  FROM server s, user u, servergroup g \n" + \
          " WHERE s.user_id = u.id AND s.servergroup_id = g.id \n" + \
          "   AND u.email LIKE ? \n" + \
          "ORDER BY 1, 2, 3"
    c.execute(sql, [p_email])
    svr_list = c.fetchall()
    svrs = []
    for row in svr_list:
      svr_dict = {}
      svr_dict['email'] = str(row[0])
      svr_dict['server_group'] = str(row[1])
      svr_dict['server_name'] = str(row[2])
      svr_dict['host'] = str(row[3])
      svr_dict['port'] = str(row[4])
      svr_dict['db'] = str(row[5])
      svr_dict['db_user'] = str(row[6])
      svr_dict['sid'] = str(row[7])
      svr_dict['gid'] = str(row[8])
      has_pwd = False
      if row[9]:
        has_pwd = True
      svr_dict['has_pwd'] = has_pwd
      svrs.append(svr_dict)
  except Exception as error:
    msg = "pgDevOps must be installed & initialized."
    util.exit_message(msg, 1, p_isJSON)

  keys = ['email', 'server_group', 'server_name', 'host', 'port', 'db', 'db_user' ]
  headers = ['Email Address', 'Server Group', 'Server Name', 'Host', 'Port', 'DB', 'DB User' ]

  if p_isJSON:
    print(json.dumps(svrs, sort_keys=True, indent=2))
  else:
    print("")
    print(api.format_data_to_table(svrs, keys, headers))

  return(0)


def is_in_pglist(p_email, p_server_group, p_server_name, p_host, p_port, p_db, p_db_user):
  try:
    c = con.cursor()
    sql = "SELECT count(*) \n" + \
          "  FROM server s, user u, servergroup g \n" + \
          " WHERE s.user_id = u.id AND s.servergroup_id = g.id \n" + \
          "   AND u.email = ? AND g.name = ? AND s.name = ? AND s.host = ? \n" + \
          "   AND s.port = ? AND s.maintenance_db = ? AND s.username = ?"
    c.execute(sql, [p_email, p_server_group, p_server_name, p_host, p_port, p_db, p_db_user])
    data = c.fetchone()
    if data[0] > 0:
      return True
  except sqlite3.Error as e:
    meta.fatal_sql_error(e, sql, "is_in_pglist()")
  
  return False 


def verify_ami(p_isJSON, p_ami):
  try:
    response = urllib2.urlopen(AMI_URL + "instance-id", timeout=2)
    out = response.read()
  except Exception as e:
    util.exit_message("This doesn't appear to be an AMI", 2, p_isJSON)

  if out != p_ami:
    util.exit_message("Incorrect AMI id", 3, p_isJSON)

  # sweet success
  return(0)


def rdslist(p_isJSON, p_isVERBOSE, p_region="", p_instance="", p_email="", p_engines=["postgres"]):

  devops_lib_path = os.path.join(os.getenv("PGC_HOME"), "pgdevops", "lib")
  if os.path.exists(devops_lib_path):
    if devops_lib_path not in sys.path:
      sys.path.append(devops_lib_path)

  try:
    import boto3
  except Exception as error:
    msg = "AWS boto3 api not installed.  \n" + str(error)
    util.exit_message(msg, 1, p_isJSON)

  if p_region is None:
    p_region = ""
  if p_instance is None:
    p_instance = ""

  try:
    rds_regions=[]
    available_rds_regions = boto3.session.Session().get_available_regions("rds")
    if p_region > "":
      if p_region in available_rds_regions:
        rds_regions = [p_region]
      else:
        msg = str(p_region) + " is not a valid region for rds."
        util.exit_message(msg, 1, p_isJSON)
    else:
      rds_regions = available_rds_regions

    # get all of the postgres db instances
    pg_list = []
    for region in rds_regions:
      msg = "Searching " + region + "..."
      util.message(msg, "info", p_isJSON)
      rds = boto3.client('rds', region_name=region)

      dbs = rds.describe_db_instances()
      for db in dbs['DBInstances']:
          if db['Engine'] in p_engines:
            pg_dict = {}
            pg_dict['engine'] = get_field(db, 'Engine')
            pg_dict['region'] = region

            pg_dict['instance'] = get_field(db, 'DBInstanceIdentifier')
            if p_instance > "":
              if p_instance != pg_dict['instance']:
                continue

            pg_dict['master_user'] = get_field(db, 'MasterUsername')
            pg_dict['status'] = get_field(db, 'DBInstanceStatus')
            pg_dict['address'] = get_field(db, 'Endpoint', 'Address')
            pg_dict['port'] = get_field(db, 'Endpoint', 'Port')
            pg_dict['dbname'] = get_field(db, 'DBName')

            pg_dict['db_class'] = get_field(db, 'DBInstanceClass')
            pg_dict['engine_version'] = get_field(db, 'EngineVersion')
            pg_dict['auto_minor_upgrade'] = get_field(db, 'AutoMinorVersionUpgrade')
            pg_dict['character_set'] = get_field(db, 'CharacterSetName')
            pg_dict['create_time'] = get_field(db, 'InstanceCreateTime')
            pg_dict['iops'] = get_field(db, 'Iops')

            pg_dict['storage_allocated'] = get_field(db, 'AllocatedStorage')
            pg_dict['storage_type'] = get_field(db, 'StorageType')
            pg_dict['storage_encrypted'] = get_field(db, 'StorageEncrypted')

            pg_dict['maint_window'] = get_field(db, 'PreferredMaintenanceWindow')
            pg_dict['backup_window'] = get_field(db, 'PreferredBackupWindow')
            pg_dict['backup_retention'] = get_field(db, 'BackupRetentionPeriod')
            pg_dict['latest_restorable'] = get_field(db, 'LatestRestorableTime')

            pg_dict['az_is_multi'] = get_field(db, 'MultiAZ')
            pg_dict['az_primary'] = get_field(db, 'AvailabilityZone')
            pg_dict['az_secondary'] = get_field(db, 'SecondaryAvailabilityZone')

            pg_dict['monitoring_interval'] = get_field(db, 'MonitoringInterval')
            pg_dict['slave_list'] = get_field(db, 'ReadReplicaDBInstanceIdentifiers')
            pg_dict['master'] = get_field(db, 'ReadReplicaSourceDBInstanceIdentifier')

            try:
              pg_dict['is_in_pglist'] = is_in_pglist(p_email, pg_dict['region'],
                pg_dict['instance'], pg_dict['address'], pg_dict['port'],
                pg_dict['dbname'], pg_dict['master_user'])
            except Exception as e:
              pass
            pg_list.append(pg_dict)
  except KeyboardInterrupt as e:
    util.exit_message("Keyboard Interrupt", 1, p_isJSON)
  except Exception as e:
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)
    msg = "Unable to run rds.describe_db_instances().  \n" + str(e)
    util.exit_message(msg, 1, p_isJSON)

  if p_isJSON:
    json_dict = {}
    json_dict['data'] = pg_list
    json_dict['state'] = 'completed'
    print(json.dumps([json_dict]))
    return(0)

  if p_isVERBOSE:
    print_verbose(pg_list)
    return(0)

  keys    = ['region', 'instance', 'status', 'dbname', 'master_user', 'port', 'address']
  headers = ['Region', 'Instance', 'Status', 'DBName', 'MasterUser',  'Port', 'Address']
  print("")
  print(api.format_data_to_table(pg_list, keys, headers))

  return(0)


def print_verbose(p_list):
  kount = 0
  for pg_dict in p_list:
    kount = kount + 1
    print("################################################")
    print("#             region: " + pg_dict['region'])
    print("#           instance: " + pg_dict['instance'])
    print("#             status: " + pg_dict['status'])
    print("#             dbname: " + pg_dict['dbname'])
    print("#        master_user: " + pg_dict['master_user'])
    print("#               port: " + pg_dict['port'])
    print("#            address: " + pg_dict['address'])
    print("#           db_class: " + pg_dict['db_class'])
    print("#     engine_version: " + pg_dict['engine_version'])
    print("# auto_minor_upgrade: " + pg_dict['auto_minor_upgrade'])
    print("#      character_set: " + pg_dict['character_set'])
    print("#        create_time: " + pg_dict['create_time'])
    print("#               iops: " + pg_dict['iops'])
    print("#  storage_allocated: " + pg_dict['storage_allocated'])
    print("#       storage_type: " + pg_dict['storage_type'])
    print("#  storage_encrypted: " + pg_dict['storage_encrypted'])
    print("#       maint_window: " + pg_dict['maint_window'])
    print("#      backup_window: " + pg_dict['backup_window'])
    print("#   backup_retention: " + pg_dict['backup_retention'])
    print("#  latest_restorable: " + pg_dict['latest_restorable'])
    print("#        az_is_multi: " + pg_dict['az_is_multi'])
    print("#         az_primary: " + pg_dict['az_primary'])
    print("#       az_secondary: " + pg_dict['az_secondary'])
    print("#   monitor_interval: " + pg_dict['monitoring_interval'])
    print("#         slave_list: " + pg_dict['slave_list'])
    print("#             master: " + pg_dict['master'])

  if kount > 0:
    print("################################################")

  return(kount)


def dbcreate(p_isJSON, p_dict):
  print("dbaas.dbcreate() 2nd pass")
  response = client.create_db_instance(
    DBName = p_dict['dbname'],
    DBInstanceIdentifier = p_dict['instance'],
    AllocatedStorage = p_dict['storage_allocated'],
    DBInstanceClass = p_dict['db_class'],
    Engine = p_dict['engine_version'],
    MasterUsername = p_dict['master_user'],
    MasterUserPassword = p_dict['password'],
    DBSecurityGroups=[
        'string',
    ],
    VpcSecurityGroupIds=[
        'string',
    ],
    AvailabilityZone='string',
    DBSubnetGroupName='string',
    PreferredMaintenanceWindow='string',
    DBParameterGroupName='string',
    BackupRetentionPeriod = 5,
    PreferredBackupWindow = 'string',
    Port = p_dict['port'],
    MultiAZ = False,
    EngineVersion = pg_dict['engine_version'],
    AutoMinorVersionUpgrade = True,
    LicenseModel='string',
    Iops=123,
    OptionGroupName='string',
    CharacterSetName='string',
    PubliclyAccessible=True|False,
    Tags=[
        {
            'Key': 'string',
            'Value': 'string'
        },
    ],
    DBClusterIdentifier='string',
    StorageType='string',
    TdeCredentialArn='string',
    TdeCredentialPassword='string',
    StorageEncrypted=False,
    KmsKeyId='string',
    Domain='string',
    CopyTagsToSnapshot=True|False,
    MonitoringInterval=123,
    MonitoringRoleArn='string',
    DomainIAMRoleName='string',
    PromotionTier=123,
    Timezone='string',
    EnableIAMDatabaseAuthentication=True|False
  )
  return(0)


def dbdumprest(p_isJSON, p_cmd, p_dbname, p_host, p_port, p_user, p_file, p_format, p_options, p_passwd="",
               p_dbtype="pg"):
  f_options = " ".join(p_options)
  if p_dbtype != "pg":
    msg = p_dbtype + " is an invalid dbtype for dbaas.dbdumprest()"
    util.exit_message(msg, 1, p_isJSON)

  if p_cmd == "dbdump":
    cmd = "pg_dump"
  elif p_cmd == "dbrestore" and 'p' == p_format:
    cmd = "psql"
  elif p_cmd == "dbrestore":
    cmd = "pg_restore"
  else:
    msg = p_cmd + " is an invalid command for dbaas.dbdumprest()"
    util.exit_message(msg, 1, p_isJSON)
  cmd = cmd + " -w -d " + str(p_dbname)
  cmd = cmd + " -h " + str(p_host)
  cmd = cmd + " -p " + str(p_port)
  cmd = cmd + " -U " + str(p_user)
  if p_cmd == "dbrestore" and 'p' != p_format:
    cmd = cmd + ' "' + str(p_file) + '"'
  else:
    cmd = cmd + ' -f "' + str(p_file) + '"'
  cmd = cmd + " -F " + str(p_format)
  cmd = cmd + " " + str(f_options)
  if p_passwd != "*":
    os.environ['PGPASSWORD'] = p_passwd

  util.message(cmd, "info", p_isJSON)
  os.system(cmd)

  return (0)
 
